package com.unud.raremelajah;

import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.IOException;

public class AksaraSuaraActivity extends AppCompatActivity {
    protected Cursor cursor;
    DataHelper dbcenter;
    ImageView prev,next,gambar;
    TextView nama;
    RelativeLayout Rlayout;
    MediaPlayer player;
    Button play;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        setContentView(R.layout.activity_aksara_suara);

        play = (Button)findViewById(R.id.play);
        nama = (TextView)findViewById(R.id.nama);
        prev = (ImageView) findViewById(R.id.prev);
        next = (ImageView) findViewById(R.id.next);
        gambar = (ImageView)findViewById(R.id.gambar);
        Rlayout = (RelativeLayout) findViewById(R.id.layout);

        dbcenter = new DataHelper(this);
        SQLiteDatabase db = dbcenter.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM tb_aksara_suara",null);
        cursor.moveToFirst();

        initialize();

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                player.start();
            }
        });

        prev.setVisibility(View.GONE);
        final Handler handler = new Handler();
        prev.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        prev.setImageBitmap(Resource.decodeSampledBitmapFromResource(getResources(), getResources().getIdentifier("prev2", "drawable", v.getContext().getPackageName()),100,100));
                        break;
                    }
                    case MotionEvent.ACTION_UP:{
                        prev.setImageBitmap(Resource.decodeSampledBitmapFromResource(getResources(), getResources().getIdentifier("pref1", "drawable", v.getContext().getPackageName()),100,100));
                        prev.setVisibility(View.GONE);
                        next.setVisibility(View.GONE);
                        Animation LeftSwipe = AnimationUtils.loadAnimation(v.getContext(), R.anim.right_slide);
                        Rlayout.startAnimation(LeftSwipe);
                        cursor.moveToPrevious();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (cursor.isFirst()){
                                    prev.setVisibility(View.GONE);
                                    next.setVisibility(View.VISIBLE);
                                }else{
                                    prev.setVisibility(View.VISIBLE);
                                    next.setVisibility(View.VISIBLE);
                                }
                            }
                        }, 501);
                        initialize();
                        break;
                    }
                }
                return true;
            }
        });

        next.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        next.setImageBitmap(Resource.decodeSampledBitmapFromResource(getResources(), getResources().getIdentifier("next2", "drawable", v.getContext().getPackageName()),100,100));
                        break;
                    }
                    case MotionEvent.ACTION_UP:{
                        next.setImageBitmap(Resource.decodeSampledBitmapFromResource(getResources(), getResources().getIdentifier("next1", "drawable", v.getContext().getPackageName()),100,100));
                        prev.setVisibility(View.GONE);
                        next.setVisibility(View.GONE);
                        Animation RightSwipe = AnimationUtils.loadAnimation(v.getContext(), R.anim.left_slide);
                        Rlayout.startAnimation(RightSwipe);
                        cursor.moveToNext();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if(cursor.isLast()){
                                    next.setVisibility(View.GONE);
                                    prev.setVisibility(View.VISIBLE);
                                }else{
                                    prev.setVisibility(View.VISIBLE);
                                    next.setVisibility(View.VISIBLE);
                                }
                            }
                        }, 501);
                        initialize();
                        break;
                    }
                }
                return true;
            }
        });
    }
    public void initialize(){
        nama.setText(cursor.getString(1));
        gambar.setImageBitmap(Resource.decodeSampledBitmapFromResource(getResources(), getResources().getIdentifier(cursor.getString(2), "drawable", this.getApplicationContext().getPackageName()), 210, 240));
        AssetFileDescriptor afd = null;
        try {
            afd = getAssets().openFd("music/"+cursor.getString(1)+".m4a");
        } catch (IOException e) {
            e.printStackTrace();
        }
        player = new MediaPlayer();
        try {
            player.setDataSource(afd.getFileDescriptor(),afd.getStartOffset(),afd.getLength());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            player.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
