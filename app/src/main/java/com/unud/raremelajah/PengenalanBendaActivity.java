package com.unud.raremelajah;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;


/**
 * Created by Ecek on 7/11/2017.
 */

public class PengenalanBendaActivity extends AppCompatActivity implements View.OnTouchListener {
    protected Cursor cursor, curBgAndMask;
    DataHelper dbcenter;

    Boolean fullDescOn = false;

    ImageView ivBenda, ivMapping, ivBendaFull;
    FrameLayout frameLayout;
    TextView tvCaption;
    Button btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        setContentView(R.layout.activity_pengenalan_benda);

        btnBack = (Button)findViewById(R.id.btnBack);
        ivBenda = (ImageView) findViewById(R.id.ivBenda);
        ivMapping = (ImageView) findViewById(R.id.ivMapping);
        if (ivBenda != null) {
            ivBenda.setOnTouchListener(this);
        }

        frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
        ivBendaFull = (ImageView) findViewById(R.id.ivBendaFull);
        tvCaption = (TextView) findViewById(R.id.tvCaption);
        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/chawp.ttf");
        tvCaption.setTypeface(custom_font);

        int page = getIntent().getIntExtra("page", 1);
        dbcenter = new DataHelper(this);
        SQLiteDatabase db = dbcenter.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM tb_pengenalan_benda where tipe=3 and page=" + page,null);
        cursor.moveToFirst();
        curBgAndMask = db.rawQuery("SELECT * FROM tb_pengenalan_benda WHERE (tipe = 1 or tipe = 2) and page = " + page, null);
        curBgAndMask.moveToFirst();

        Picasso.with(this).load(getResources().getIdentifier(curBgAndMask.getString(2), "drawable", getPackageName())).into(ivBenda);
        curBgAndMask.moveToNext();
        Picasso.with(this).load(getResources().getIdentifier(curBgAndMask.getString(2), "drawable", getPackageName())).into(ivMapping);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public boolean onTouch(View v, MotionEvent ev) {

        final int action = ev.getAction();

        final int evX = (int) ev.getX();
        final int evY = (int) ev.getY();

        switch (action) {
            case MotionEvent.ACTION_DOWN :

                break;

            case MotionEvent.ACTION_UP :
                if (!fullDescOn){
                    int touchColor = getHotspotColor(R.id.ivMapping, evX, evY);
                    Log.i("sss", String.valueOf(touchColor));
                    cursor.moveToFirst();
                    while(!cursor.isAfterLast()){
                        Log.i("sss", String.valueOf(touchColor) + " " + String.valueOf(cursor.getInt(3)));
                        if(touchColor == cursor.getInt(3)){
                            tvCaption.setText(cursor.getString(5));
                            frameLayout.setVisibility(View.VISIBLE);
                            Picasso.with(this).load(getResources().getIdentifier(cursor.getString(2), "drawable", v.getContext().getPackageName())).into(ivBendaFull);
                            ivBendaFull.setMinimumHeight(500);
                            fullDescOn = true;
                            break;
                        }
                        cursor.moveToNext();
                    }
                } else {
                    frameLayout.setVisibility(View.GONE);
                    fullDescOn = false;
                }
                break;
            default:

        }

        return true;
    }

    public int getHotspotColor (int hotspotId, int x, int y) {
        ImageView img = (ImageView) findViewById (hotspotId);
        img.setDrawingCacheEnabled(true);
        Bitmap hotspots = Bitmap.createBitmap(img.getDrawingCache());
        img.setDrawingCacheEnabled(false);
        return hotspots.getPixel(x, y);
    }
}

