package com.unud.raremelajah;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import com.kyanogen.signatureview.SignatureView;

public class ItemDetailActivity extends AppCompatActivity {
    ImageView home,restart,gambar_menulis;
    DataHelper dbcenter;
    protected Cursor cursor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        setContentView(R.layout.activity_membaca_hewan);
        setContentView(R.layout.activity_item_detail);
        home = (ImageView)findViewById(R.id.home);
        restart = (ImageView)findViewById(R.id.restart);
        gambar_menulis = (ImageView)findViewById(R.id.gambar_tulis);
        final SignatureView signatureView = (SignatureView) findViewById(R.id.signature_view);

        dbcenter = new DataHelper(this);
        SQLiteDatabase db = dbcenter.getReadableDatabase();
        cursor = db.rawQuery("SELECT gambar FROM tb_menulis where nama='"+getIntent().getStringExtra("nama")+"'",null);
        cursor.moveToFirst();

        gambar_menulis.setImageResource(getResources().getIdentifier(cursor.getString(0), "drawable", this.getApplicationContext().getPackageName()));

        home.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        home.setImageBitmap(Resource.decodeSampledBitmapFromResource(getResources(), getResources().getIdentifier("home2", "drawable", v.getContext().getPackageName()),100,100));
                        break;
                    }
                    case MotionEvent.ACTION_UP:{
                        home.setImageBitmap(Resource.decodeSampledBitmapFromResource(getResources(), getResources().getIdentifier("home1", "drawable", v.getContext().getPackageName()),100,100));
                        finish();
                        FragmentManager fm = getSupportFragmentManager();
                        fm.popBackStack();
                        break;
                    }
                }
                return true;
            }
        });

        restart.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        restart.setImageBitmap(Resource.decodeSampledBitmapFromResource(getResources(), getResources().getIdentifier("restart2", "drawable", v.getContext().getPackageName()),100,100));
                        break;
                    }
                    case MotionEvent.ACTION_UP:{
                        restart.setImageBitmap(Resource.decodeSampledBitmapFromResource(getResources(), getResources().getIdentifier("restart1", "drawable", v.getContext().getPackageName()),100,100));
                        signatureView.clearCanvas();
                        break;
                    }
                }
                return true;
            }
        });
    }
}
