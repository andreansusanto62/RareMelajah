package com.unud.raremelajah;

import android.app.Service;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.IBinder;

import java.io.IOException;

/**
 * Created by andre on 07/12/2017.
 */

public class BgmService extends Service {
    private static final String TAG = null;
    MediaPlayer player;
    public IBinder onBind(Intent arg0) {

        return null;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        AssetFileDescriptor afd = null;
        try {
            afd = getAssets().openFd("music/back_sound.mp3");
        } catch (IOException e) {
            e.printStackTrace();
        }
        player = new MediaPlayer();
        try {
            player.setDataSource(afd.getFileDescriptor(),afd.getStartOffset(),afd.getLength());
        } catch (IOException e) {
            e.printStackTrace();
        }
        player.setLooping(true); // Set looping
        player.setVolume(70,70);
        try {
            player.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public int onStartCommand(Intent intent, int flags, int startId) {
        player.start();
        return START_STICKY;
    }

    public void onStart(Intent intent, int startId) {
        // TO DO
    }
    public IBinder onUnBind(Intent arg0) {
        // TO DO Auto-generated method
        return null;
    }

    public void onStop() {
        stopService(new Intent(this, BgmService.class));
    }
    public void onPause() {
        stopService(new Intent(this, BgmService.class));
    }
    @Override
    public void onDestroy() {
        stopService(new Intent(this, BgmService.class));
        player.stop();
        player.release();
    }

    @Override
    public void onLowMemory() {

    }
}
