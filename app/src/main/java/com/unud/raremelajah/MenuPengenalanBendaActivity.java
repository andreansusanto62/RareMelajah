package com.unud.raremelajah;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Created by Ecek on 7/12/2017.
 */

public class MenuPengenalanBendaActivity extends AppCompatActivity {
    Button btnHome;
    ImageView option1, option2, option3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        setContentView(R.layout.activity_menu_pengenalan_benda);

        option1 = (ImageView)findViewById(R.id.option1);
        option2 = (ImageView)findViewById(R.id.option2);
        option3 = (ImageView)findViewById(R.id.option3);
        btnHome = (Button)findViewById(R.id.btnHome);

        Picasso.with(this).load(R.drawable.pengenalan_benda_bg1).resize(200, 200).centerCrop().into(option1);
        Picasso.with(this).load(R.drawable.pengenalan_benda_bg2).resize(200, 200).centerCrop().into(option2);
        Picasso.with(this).load(R.drawable.pengenalan_benda_bg3).resize(200, 200).centerCrop().into(option3);

        TextView tvOption1 = (TextView)findViewById(R.id.tvOption1);
        TextView tvOption2 = (TextView)findViewById(R.id.tvOption2);
        TextView tvOption3 = (TextView)findViewById(R.id.tvOption3);
        TextView tvTitle = (TextView)findViewById(R.id.tvTitle);
        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/chawp.ttf");
        tvOption1.setTypeface(custom_font);
        tvOption2.setTypeface(custom_font);
        tvOption3.setTypeface(custom_font);
        tvTitle.setTypeface(custom_font);

        final Intent intent = new Intent(getApplicationContext(), PengenalanBendaActivity.class);
        option1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.putExtra("page", 1);
                startActivity(intent);
            }
        });
        option2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.putExtra("page", 2);
                startActivity(intent);
            }
        });
        option3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.putExtra("page", 3);
                startActivity(intent);
            }
        });
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
