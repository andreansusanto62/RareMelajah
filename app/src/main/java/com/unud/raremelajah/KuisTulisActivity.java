package com.unud.raremelajah;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;


public class KuisTulisActivity extends AppCompatActivity {

    protected Cursor curQuestion;
    DataHelper dbcenter;

    EditText etJawaban;
    ImageView ivKuis, ivBadge;
    Button btnBack, btnOk;
    TextView tvScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        setContentView(R.layout.activity_kuis_menulis);

        etJawaban = (EditText)findViewById(R.id.etJawaban);
        ivKuis = (ImageView)findViewById(R.id.ivKuis);
        ivBadge = (ImageView)findViewById(R.id.ivBadge);
        btnBack = (Button)findViewById(R.id.btnBack);
        btnOk = (Button)findViewById(R.id.btnOk);
        tvScore = (TextView)findViewById(R.id.tvScore);

        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/chawp.ttf");
        etJawaban.setTypeface(custom_font);
        tvScore.setTypeface(custom_font);

        MediaPlayer intro = MediaPlayer.create(this, R.raw.intro_kuis_menulis_membaca);
        intro.start();

        dbcenter = new DataHelper(this);
        SQLiteDatabase db = dbcenter.getReadableDatabase();
        curQuestion = db.rawQuery("SELECT * FROM tb_kuis_menulis order by random() limit 5",null);
        curQuestion.moveToFirst();

        Picasso.with(this).load(getResources().getIdentifier(curQuestion.getString(1), "drawable", getPackageName())).resize(288, 162).centerCrop().into(ivKuis);
        
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkAnswer();
            }
        });

    }

    private void checkAnswer() {

        if (etJawaban.getText().toString().equals(curQuestion.getString(2))){
            Picasso.with(this).load(R.drawable.badge_benar).into(ivBadge);
            Log.i("sss", tvScore.getText().toString());
            int score = Integer.parseInt(tvScore.getText().toString()) + 10;
            tvScore.setText(String.valueOf(score));
        } else {
            Picasso.with(this).load(R.drawable.badge_salah).into(ivBadge);
        }

        new CountDownTimer(2000, 1000) {
            public void onTick(long millisUntilFinished) {
                ivBadge.setVisibility(View.VISIBLE);
            }
            public void onFinish() {
                ivBadge.setVisibility(View.INVISIBLE);
                curQuestion.moveToNext();
                Picasso.with(getApplicationContext())
                    .load(getResources().getIdentifier(curQuestion.getString(1), "drawable", getPackageName()))
                    .resize(288, 162)
                    .centerCrop()
                    .into(ivKuis);
                etJawaban.setText("");
            }
        }.start();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        immersiveMode();
        getWindow().getDecorView().setOnSystemUiVisibilityChangeListener
            (new View.OnSystemUiVisibilityChangeListener() {
                @Override
                public void onSystemUiVisibilityChange(int visibility) {
                    immersiveMode();
                }
            });
    }

    public void immersiveMode() {
        final View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }
}
