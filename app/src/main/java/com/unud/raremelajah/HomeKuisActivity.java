package com.unud.raremelajah;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class HomeKuisActivity extends AppCompatActivity {
    Button tulis,baca,hitung;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        setContentView(R.layout.activity_membaca_hewan);
        setContentView(R.layout.activity_home_kuis);

        TextView label = (TextView)findViewById(R.id.label);
        label.setTypeface(Typeface.createFromAsset(getAssets(),  "fonts/chawp.ttf"));
        tulis = (Button) findViewById(R.id.tulis);
        baca = (Button) findViewById(R.id.baca);
        hitung = (Button) findViewById(R.id.hitung);

        tulis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(getApplicationContext(), KuisTulisActivity.class));
            }
        });
        baca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(getApplicationContext(), KuisBacaActivity.class));
            }
        });
        hitung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(getApplicationContext(), KuisHitungActivity.class));
            }
        });
    }
}
