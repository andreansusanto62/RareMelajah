package com.unud.raremelajah;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class HomeMembacaActivity extends AppCompatActivity {
    Button membacaBuah,membacaHewan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        setContentView(R.layout.activity_home_membaca);

        TextView label = (TextView)findViewById(R.id.label);
        label.setTypeface(Typeface.createFromAsset(getAssets(),  "fonts/chawp.ttf"));
        membacaBuah = (Button) findViewById(R.id.membaca_buah);
        membacaHewan = (Button) findViewById(R.id.membaca_hewan);
        /*final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Loading data, Please wait..");
        dialog.show();
        dialog.dismiss();*/
        membacaBuah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MembacaBuahActivity.class));
            }
        });
        membacaHewan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MembacaHewanActivity.class));
            }
        });
    }
}
