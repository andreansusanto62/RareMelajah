package com.unud.raremelajah;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


public class HomeHitungActivity extends AppCompatActivity {
    protected Cursor cursor;
    DataHelper dbcenter;
    ImageView gambar1,gambar2;
    ImageView next,prev;
    LinearLayout Rlayout;
    TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        setContentView(R.layout.activity_home_hitung);

        tv=(TextView)findViewById(R.id.tv);
        gambar1 = (ImageView)findViewById(R.id.gambar1);
        gambar2 = (ImageView)findViewById(R.id.gambar2);
        prev = (ImageView) findViewById(R.id.prev);
        next = (ImageView) findViewById(R.id.next);

        Rlayout = (LinearLayout) findViewById(R.id.layout);
        tv.setTypeface(Typeface.createFromAsset(getAssets(),  "fonts/chawp.ttf"));
        dbcenter = new DataHelper(this);
        SQLiteDatabase db = dbcenter.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM tb_hitung",null);
        cursor.moveToFirst();

        gambar1.setImageBitmap(Resource.decodeSampledBitmapFromResource(getResources(), getResources().getIdentifier(cursor.getString(1), "drawable", this.getApplicationContext().getPackageName()), 210, 240));
        gambar2.setImageBitmap(Resource.decodeSampledBitmapFromResource(getResources(), getResources().getIdentifier(cursor.getString(2), "drawable", this.getApplicationContext().getPackageName()), 210, 240));
        prev.setVisibility(View.GONE);
        final Handler handler = new Handler();
        prev.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        prev.setImageBitmap(Resource.decodeSampledBitmapFromResource(getResources(), getResources().getIdentifier("prev2", "drawable", v.getContext().getPackageName()),100,100));
                        break;
                    }
                    case MotionEvent.ACTION_UP:{
                        prev.setImageBitmap(Resource.decodeSampledBitmapFromResource(getResources(), getResources().getIdentifier("pref1", "drawable", v.getContext().getPackageName()),100,100));
                        prev.setVisibility(View.GONE);
                        next.setVisibility(View.GONE);
                        Animation LeftSwipe = AnimationUtils.loadAnimation(v.getContext(), R.anim.right_slide);
                        Rlayout.startAnimation(LeftSwipe);
                        cursor.moveToPrevious();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (cursor.isFirst()){
                                    prev.setVisibility(View.GONE);
                                    next.setVisibility(View.VISIBLE);
                                }else{
                                    prev.setVisibility(View.VISIBLE);
                                    next.setVisibility(View.VISIBLE);
                                }
                            }
                        }, 501);
                        gambar1.setImageBitmap(Resource.decodeSampledBitmapFromResource(getResources(), getResources().getIdentifier(cursor.getString(1), "drawable", v.getContext().getPackageName()), 210, 240));
                        gambar2.setImageBitmap(Resource.decodeSampledBitmapFromResource(getResources(), getResources().getIdentifier(cursor.getString(2), "drawable", v.getContext().getPackageName()), 210, 240));
                        break;
                    }
                }
                return true;
            }
        });

        next.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        next.setImageBitmap(Resource.decodeSampledBitmapFromResource(getResources(), getResources().getIdentifier("next2", "drawable", v.getContext().getPackageName()),100,100));
                        break;
                    }
                    case MotionEvent.ACTION_UP:{
                        next.setImageBitmap(Resource.decodeSampledBitmapFromResource(getResources(), getResources().getIdentifier("next1", "drawable", v.getContext().getPackageName()),100,100));
                        prev.setVisibility(View.GONE);
                        next.setVisibility(View.GONE);
                        Animation RightSwipe = AnimationUtils.loadAnimation(v.getContext(), R.anim.left_slide);
                        Rlayout.startAnimation(RightSwipe);
                        cursor.moveToNext();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if(cursor.isLast()){
                                    next.setVisibility(View.GONE);
                                    prev.setVisibility(View.VISIBLE);
                                }else{
                                    prev.setVisibility(View.VISIBLE);
                                    next.setVisibility(View.VISIBLE);
                                }
                            }
                        }, 501);
                        gambar1.setImageBitmap(Resource.decodeSampledBitmapFromResource(getResources(), getResources().getIdentifier(cursor.getString(1), "drawable", v.getContext().getPackageName()), 210, 240));
                        gambar2.setImageBitmap(Resource.decodeSampledBitmapFromResource(getResources(), getResources().getIdentifier(cursor.getString(2), "drawable", v.getContext().getPackageName()), 210, 240));
                        break;
                    }
                }
                return true;
            }
        });
    }
}
