package com.unud.raremelajah;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;


public class HomeMenulisActivity extends ActionBarActivity {
    GridView gridView;
    protected Cursor cursor;
    DataHelper dataHelper;
    int[] item;
    String[] nama;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        setContentView(R.layout.activity_membaca_hewan);
        setContentView(R.layout.activity_home_menulis);

        dataHelper = new DataHelper(this);
        SQLiteDatabase db = dataHelper.getReadableDatabase();
        cursor = db.rawQuery("SELECT gambar,nama FROM tb_membaca order by abjad asc",null);

        item = new int[cursor.getCount()];
        nama = new String[cursor.getCount()];
        int i =0;
        if (cursor != null) {
            while(cursor.moveToNext()) {
                item[i] = getResources().getIdentifier(cursor.getString(0), "drawable", this.getApplicationContext().getPackageName());
                nama[i] = cursor.getString(1);
                i++;
            }
            cursor.close();
        }

        CustomGrid adapter = new CustomGrid(HomeMenulisActivity.this, item);
        gridView=(GridView)findViewById(R.id.grid);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent intent = new Intent(HomeMenulisActivity.this, ItemDetailActivity.class);
                intent.putExtra("nama", nama[position]);
                startActivity(intent);
                //Toast.makeText(HomeMenulisActivity.this, "You Clicked at " +nama[position], Toast.LENGTH_SHORT).show();
                //StartActivity(new Intent(getApplicationContext(), ItemDetailActivity.class));


            }
        });
    }
}
