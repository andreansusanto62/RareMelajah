package com.unud.raremelajah;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class HomeAksaraActivity extends AppCompatActivity {
    Button aksaraSuara,aksaraWianjana,aksaraWilangan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        setContentView(R.layout.activity_home_aksara);

        TextView label = (TextView)findViewById(R.id.label);
        label.setTypeface(Typeface.createFromAsset(getAssets(),  "fonts/chawp.ttf"));
        aksaraSuara = (Button) findViewById(R.id.aksara_suara);
        aksaraWianjana = (Button) findViewById(R.id.aksara_wianjana);
        aksaraWilangan = (Button) findViewById(R.id.aksara_wilangan);

        aksaraSuara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), AksaraSuaraActivity.class));
            }
        });
        aksaraWianjana.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), AksaraWianjanaActivity.class));
            }
        });
        aksaraWilangan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), AksaraWilanganActivity.class));
            }
        });
    }
}
