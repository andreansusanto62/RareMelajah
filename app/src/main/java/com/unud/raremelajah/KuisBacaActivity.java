package com.unud.raremelajah;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


public class KuisBacaActivity extends AppCompatActivity implements View.OnClickListener {
    protected Cursor cursor;
    ImageView imageSoal,jawabanA,jawabanB,jawabanC,jawabanD,status;
    DataHelper dbcenter;
    FrameLayout frameLayout,frameLayoutScore;
    LinearLayout mainLayout;
    TextView tvScore;
    Button okScore;
    int score = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        setContentView(R.layout.activity_membaca_hewan);
        setContentView(R.layout.activity_kuis_baca);

        mainLayout = (LinearLayout)findViewById(R.id.main_layout);
        frameLayout =(FrameLayout)findViewById(R.id.frameLayout);
        frameLayoutScore = (FrameLayout)findViewById(R.id.frameLayoutScore);
        tvScore = (TextView)findViewById(R.id.tvScore);
        okScore = (Button)findViewById(R.id.ok_score);
        imageSoal = (ImageView) findViewById(R.id.image_soal);
        jawabanA = (ImageView)findViewById(R.id.jawabanA);
        jawabanB = (ImageView)findViewById(R.id.jawabanB);
        jawabanC = (ImageView)findViewById(R.id.jawabanC);
        jawabanD = (ImageView)findViewById(R.id.jawabanD);
        status = (ImageView)findViewById(R.id.status);

        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/chawp.ttf");
        tvScore.setTypeface(custom_font);

        frameLayout.setVisibility(View.INVISIBLE);
        frameLayoutScore.setVisibility(View.INVISIBLE);
        dbcenter = new DataHelper(this);
        SQLiteDatabase db = dbcenter.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM tb_kuis_baca order by RANDOM() LIMIT 10",null);
        cursor.moveToFirst();

        initialize();

        jawabanA.setOnClickListener(this);
        jawabanB.setOnClickListener(this);
        jawabanC.setOnClickListener(this);
        jawabanD.setOnClickListener(this);

        okScore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(getApplicationContext(), HomeKuisActivity.class));
            }
        });
    }

    public void initialize(){
        imageSoal.setImageBitmap(roundCornerImage(BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(cursor.getString(1), "drawable", this.getApplicationContext().getPackageName())),50));
        jawabanA.setTag(getResources().getIdentifier(cursor.getString(2), "drawable", this.getApplicationContext().getPackageName()));
        jawabanA.setImageResource(getResources().getIdentifier(cursor.getString(2), "drawable", this.getApplicationContext().getPackageName()));
        jawabanB.setTag(getResources().getIdentifier(cursor.getString(3), "drawable", this.getApplicationContext().getPackageName()));
        jawabanB.setImageResource(getResources().getIdentifier(cursor.getString(3), "drawable", this.getApplicationContext().getPackageName()));
        jawabanC.setTag(getResources().getIdentifier(cursor.getString(4), "drawable", this.getApplicationContext().getPackageName()));
        jawabanC.setImageResource(getResources().getIdentifier(cursor.getString(4), "drawable", this.getApplicationContext().getPackageName()));
        jawabanD.setTag(getResources().getIdentifier(cursor.getString(5), "drawable", this.getApplicationContext().getPackageName()));
        jawabanD.setImageResource(getResources().getIdentifier(cursor.getString(5), "drawable", this.getApplicationContext().getPackageName()));
    }

    public Bitmap roundCornerImage(Bitmap raw, float round) {
        int width = raw.getWidth();
        int height = raw.getHeight();
        Bitmap result = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(result);
        canvas.drawARGB(0, 0, 0, 0);

        final Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(Color.parseColor("#000000"));

        final Rect rect = new Rect(0, 0, width, height);
        final RectF rectF = new RectF(rect);

        canvas.drawRoundRect(rectF, round, round, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(raw, rect, rect, paint);

        return result;
    }

    @Override
    public void onClick(final View v) {
        v.startAnimation(AnimationUtils.loadAnimation(v.getContext(),R.anim.click_anim));
        int choice = (int) v.getTag();
        int answ = getResources().getIdentifier(cursor.getString(6), "drawable", this.getApplicationContext().getPackageName());
        if (choice==answ){
            score++;
            status.setImageDrawable(getResources().getDrawable(R.drawable.benar));
        }else{
            status.setImageDrawable(getResources().getDrawable(R.drawable.salah));
        }

        frameLayout.setVisibility(View.VISIBLE);
        //mainLayout.startAnimation(AnimationUtils.loadAnimation(v.getContext(), R.anim.fadeout));
        mainLayout.setVisibility(View.INVISIBLE);
        frameLayout.startAnimation(AnimationUtils.loadAnimation(v.getContext(), R.anim.fadein));
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable(){
            @Override
            public void run() {
                frameLayout.startAnimation(AnimationUtils.loadAnimation(v.getContext(), R.anim.fadeout));
                frameLayout.setVisibility(View.INVISIBLE);
            }
        },2000);

        if (cursor.isLast()){
            tvScore.setText(String.valueOf(score*10));
            frameLayoutScore.setVisibility(View.VISIBLE);
            frameLayoutScore.startAnimation(AnimationUtils.loadAnimation(v.getContext(), R.anim.fadein));
        }else{
            cursor.moveToNext();
            initialize();
            handler.postDelayed(new Runnable(){
                @Override
                public void run() {
                    mainLayout.setVisibility(View.VISIBLE);
                    mainLayout.startAnimation(AnimationUtils.loadAnimation(v.getContext(), R.anim.fadein));
                }
            },2000);
        }
    }
}
